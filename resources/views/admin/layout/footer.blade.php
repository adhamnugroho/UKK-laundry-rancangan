<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Beta</b> Version
    </div>
    <strong>Copyright &copy; <span id="tahun_sekarang"></span> Adham Nugroho.</strong> All
    rights reserved.
</footer>
